package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

import java.util.Date;

@Data
public class FileProvisionVO {

    private String motivo;
    private String rutCliente;
    private Long nroOperacion;
    private String pagadoPor;
    private String cuotaInicial;
    private String cuotaFinal;
    private String fechaMovimiento;
    private String sucursal;
    private String caja;
    private long ingreso;
    private String userNameActivo;
    private long userIdActivo;
}
