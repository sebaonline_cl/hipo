package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class MedioPagoVO {

    private float codMedioPago;
    private String descripcion;
}
