package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class LoginVO {

    private String access_token;
    private String token_type;
    private String refresh_token;
    private float expires_in;
    private String scope;
    private String jti;

}
