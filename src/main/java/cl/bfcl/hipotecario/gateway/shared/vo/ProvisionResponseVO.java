package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class ProvisionResponseVO {

        private String id;
        private float nroSolicitud;
        EstadoProvisionVO EstadoProvision;
        private float rut;
        private String nombreCliente;
        private String apellidoPaternoCliente;
        private String apellidoMaternoCliente;
        TipoProvisionVO TipoProvision;
        MedioPagoVO MedioPago;
        private String nroCuenta;
        private float montoProvisionUF;
        private float montoProvisionPesos;
        OrigenProvisionVO OrigenProvision;
        private String urlMandato;
        private String fechaContable;
        private String fechaInicio;
        private String fechaAsignacion;
        private String fechaProceso;
        private String fechaFinalizacion;
        private float usuarioRegistro;
}