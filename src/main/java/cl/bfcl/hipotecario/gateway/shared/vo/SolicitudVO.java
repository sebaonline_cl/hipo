package cl.bfcl.hipotecario.gateway.shared.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolicitudVO {

    private float nroSolicitud;
    private float rut;
    private String dv;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private float valorUF;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<Object> listTipoProvision = new ArrayList<>();
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<Object> listMedioPago = new ArrayList<>();
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<Object> listCuentaCliente = new ArrayList<>();
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<Object> msgWarning = new ArrayList<>();

}
