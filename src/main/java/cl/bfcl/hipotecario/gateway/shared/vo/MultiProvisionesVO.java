package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class MultiProvisionesVO {

    private String motivo;
    private String rutCliente;
    private String nroOperacion;
    private String pagadoPor;
    private String cuotaInicial;
    private String cuotaFinal;
    private String fechaMovimiento;
    private String sucursal;
    private String caja;
    private String ingreso;
}
