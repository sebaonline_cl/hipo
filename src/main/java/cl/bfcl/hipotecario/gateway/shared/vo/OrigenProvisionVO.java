package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class OrigenProvisionVO {

    private float codOrigenProvision;
    private String descripcion;

}
