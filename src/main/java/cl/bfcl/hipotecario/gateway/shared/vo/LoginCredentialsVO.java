package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class LoginCredentialsVO {

    private String username;
    private String password;
}
