package cl.bfcl.hipotecario.gateway.shared;

public class JwtConfig {

    public static final String SECRET_KEY = "";
    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAv/gfHlvZtDy+exHVIhIqiSRF9e1nlSgSPx0z9zIsihHz6lNQ\n" +
            "L7fGPIqOoA2l/+UMV9dpmqto1oAkwK4jSXqpEO05VWe4qMRnuQlqeJl4Q1tBByFA\n" +
            "OHhxPP9q1MqUBfiMLh4Zsfqw2gZOlDivrD7V07swSMoprMLBe3Otb+biwpXX17BW\n" +
            "hea8UuAmH54Qz9KoDMRdWS3Jyxp74thQiBnSrDNLman3t1OEgCNOOHEBD8nP534Y\n" +
            "aPa07l/iItcLm9RhcsJ3E36lada4HDo5SfFE0m6mVVk4MvRawZQPrIjmpwPyCY2v\n" +
            "Pk/x/buYIlPCf/lsSsuRvLz6sQdraFuieHDzFwIDAQABAoIBADYR0doALup4LYKG\n" +
            "fEyTZ02nXNku2/0EA1ephwh6JldhRR1cB/rYvUoyEIf+MCJKRla7grAg1W+NOkzW\n" +
            "/3NjzBjLaFmo1Dcc0sYoY8fCGkCGhGNYtzjwm5x9qH507Ipd/kXDDqO1o3hVTG6t\n" +
            "JLXUIO/5ClmGL4TMeiimZVReaMI7ax0fOVOG1XzPkP3npo8cb+Tjq78Iorn2pK5s\n" +
            "YcBz7ygkQ5t4lSqKOclDy2VLykGqth8Al/MfeaxPqHLyQWccEjnYM5nBR/tkFJlg\n" +
            "Bs5DBFC+XfoDMkINpBm7OYoM9DlVIIEfQxG0pAFXqYYtpJbz0p4mRW4B1fiTyou9\n" +
            "mN/PcIkCgYEA7SDVn+yOgQ6MSE6HLV0gaZXc5yCTk+OyRvZy4lvEyyu2Oi2Ko3WS\n" +
            "lUJNAAcRz0eNrhiba7wLg/mwH7gsCYN46cvfxV5EKvEixbzryhn8V+ZoKrWRuIME\n" +
            "9M5mtJxUmAIXARXiIj66aBCVkd6vs4IOzNt0IAe9FdmQeQapKmnzIS0CgYEAzz87\n" +
            "tDB34SBITYWGRkBFKue+sXjEWLvFelIim/EXB0BQFPZnPiKAl/zQLiQuJcQBY0IG\n" +
            "u8S2F18KQVVetlqSup/ZJXu+rWM4ObEp9Ry1qbROSM3udzlVH0GyT1q6KbHDUup+\n" +
            "C5rjcUvHRra0wnFk9qQSSZTVraOV6eL961qx59MCgYEAgRWKtRA1UxP0UbhN5ef4\n" +
            "KQInyrn1HGCrP+4RBCAotOEKxgjcm7b5IL+C4yz2CPxOqCNFGBS5wBJASkQb1OH5\n" +
            "BGyzV+HFAgtafoIlaVNUBxWqRvWoaeUI4asqRc+ETWt10mVzQGkdWGEOyuWKiBGY\n" +
            "tPQR9w2/ZRGWkAuMM0aRz9ECgYEAoWTP/PIl2WcHh39tYvYYJ2KUCadP2P5ZunmO\n" +
            "RDzLPzVhaAG/ww6hcDtMcDJy0axDnGXF0qJNYAP+e4SdHouIatejBmLIykRTe/Ix\n" +
            "B5ZVDpTAxkfA5o36cT37O3sfMOsjfkXo5Oa5KvjldndAa5BdHQoGx9ClWvTjAWJp\n" +
            "dciKdUsCgYB6bQn4WcPNngf+3enf1hoJNMqliDFvkpRrsHM2v3rTj8GKkr2VnXBp\n" +
            "l+orjWyyo4o4RgcRX5GsKBdtGnXopmuNpreToC6CBuTHNLufIo30bL65e7C0G3sV\n" +
            "/Zgb2s6Vzmez4LwvPqhe9nUP+QXKz5OjeTo2yihLRlKxf9JgCN18TA==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv/gfHlvZtDy+exHVIhIq\n" +
            "iSRF9e1nlSgSPx0z9zIsihHz6lNQL7fGPIqOoA2l/+UMV9dpmqto1oAkwK4jSXqp\n" +
            "EO05VWe4qMRnuQlqeJl4Q1tBByFAOHhxPP9q1MqUBfiMLh4Zsfqw2gZOlDivrD7V\n" +
            "07swSMoprMLBe3Otb+biwpXX17BWhea8UuAmH54Qz9KoDMRdWS3Jyxp74thQiBnS\n" +
            "rDNLman3t1OEgCNOOHEBD8nP534YaPa07l/iItcLm9RhcsJ3E36lada4HDo5SfFE\n" +
            "0m6mVVk4MvRawZQPrIjmpwPyCY2vPk/x/buYIlPCf/lsSsuRvLz6sQdraFuieHDz\n" +
            "FwIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
