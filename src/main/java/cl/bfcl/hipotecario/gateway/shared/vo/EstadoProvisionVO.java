package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class EstadoProvisionVO {

    private float codEstado;
    private String descripcion;
}
