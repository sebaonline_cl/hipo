package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

@Data
public class TipoProvisionVO {

    private float codTipoProvision;
    private String descripcion;

}
