package cl.bfcl.hipotecario.gateway.shared.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author ralvaradom
 *
 */
@Data
public class ProvisionVO {
	
	private String id;
	private long nroSolicitud;	
	private long rut;
	private String dv;
	
	@NotEmpty(message = "El nombre Cliente no puede estar vacio")
	private String nombreCliente;
	
	@NotEmpty(message = "El apellido paterno Cliente no puede estar vacio")
	private String apellidoPaternoCliente;
	
	@NotEmpty(message = "El apellido materno Cliente no puede estar vacio")
	private String apellidoMaternoCliente;
	
	private int estadoProvision;
	
    private int tipoProvision;
	
	private int medioPago;
	
	@NotEmpty(message = "El Nro. cuenta no puede estar vacio")
	private String nroCuenta;
	
	private float montoProvisionUF;
	
	@NotNull(message = "El monto de provision en pesos no puede ser vacio")
	private long montoProvisionPesos;
	
	private int origenProvision;
	
	@NotEmpty(message = "El mandano no puede estar vacio")
	private String urlMandato;
		
	private long usuarioRegistro;
	
	private String nombreUsuarioReg;

}
