/**
 * 
 */
package cl.bfcl.hipotecario.gateway.exception;

/**
 * @author ralvaradom
 *
 */

@SuppressWarnings("serial")
public class BadRequestException extends RuntimeException {

	private static final String DESCRIPTION = "Bad Request Exception (400)";

	public BadRequestException(String detail) {
		super(DESCRIPTION + ". " + detail);
		// TODO Auto-generated constructor stub
	}
	
	
}
