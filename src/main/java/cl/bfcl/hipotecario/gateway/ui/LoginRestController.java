package cl.bfcl.hipotecario.gateway.ui;

import cl.bfcl.hipotecario.gateway.application.LoginService;
import cl.bfcl.hipotecario.gateway.shared.vo.LoginVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.HashMap;

@RestController
public class LoginRestController {

    @Autowired
    private LoginService loginService;

    @ApiOperation(value = "Login for Application", nickname = "login",
            response = LoginVO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = LoginVO.class)})
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestParam("username") String username, @RequestParam("password") String pwd) {
        HashMap<String, Serializable> map = new HashMap<String, Serializable>();
        LoginVO loginVO = this.loginService.userLogin(username, pwd);
        if (loginVO == null) {
            map.put("error: ", "Error al registrar la provision");
            return new ResponseEntity<HashMap<String, Serializable>>(map,  HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(loginVO, HttpStatus.CREATED);
    }


}
