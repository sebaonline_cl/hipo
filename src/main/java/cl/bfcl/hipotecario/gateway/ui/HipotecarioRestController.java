package cl.bfcl.hipotecario.gateway.ui;

import cl.bfcl.hipotecario.gateway.application.ProvisionService;
import cl.bfcl.hipotecario.gateway.shared.vo.FileProvisionVO;
import cl.bfcl.hipotecario.gateway.shared.vo.ProvisionResponseVO;
import cl.bfcl.hipotecario.gateway.shared.vo.ProvisionVO;
import cl.bfcl.hipotecario.gateway.shared.vo.SolicitudVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mortgage")
@Api(tags = "request", description = "Gateway Mortgage")
public class HipotecarioRestController {

    private ProvisionService provisionService;

    @Autowired
    public HipotecarioRestController(ProvisionService provisionService) {
        this.provisionService = provisionService;
    }

    @ApiOperation(value = "Search request", nickname = "searchSolicitud",
            response = SolicitudVO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = SolicitudVO.class)})
    @Secured({"ROLE_MESA_HIPOTECARIA","ROLE_CARTERA"})
    @GetMapping("/search-request/{request}")
    public ResponseEntity<?> searchSolicitud(@PathVariable BigDecimal request)  {
        SolicitudVO solicitudProvicion = new SolicitudVO();
        Map<String, Object> response = new HashMap<>();
        try {
            solicitudProvicion = provisionService.findByNroSolicitud(request);
        }catch (Exception e){
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("error", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(solicitudProvicion == null) {
            response.put("mensaje: ", "Nro de Solicitud no existe");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<SolicitudVO>(solicitudProvicion, HttpStatus.OK);
    }

    @ApiOperation(value = "Crea Provision", nickname = "createProvision",
            response = ProvisionVO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProvisionVO.class)})
    @Secured({"ROLE_MESA_HIPOTECARIA"})
    @PostMapping("/create-provision")
    public ResponseEntity<?> createProvision(@Valid  @RequestBody ProvisionVO provision) {
        HashMap<String, Serializable> map = new HashMap<String, Serializable>();
        ProvisionVO provisionVO = this.provisionService.createProvision(provision);
        if (provisionVO == null) {
            map.put("error: ", "Error al registrar la provision");
            return new ResponseEntity<HashMap<String, Serializable>>(map,  HttpStatus.CONFLICT);
        }
        return new ResponseEntity<ProvisionVO>(provisionVO, HttpStatus.CREATED);
    }

   @ApiOperation(value = "Crea Multiple Provisiones", nickname = "createMultipleProvision",
            response = ProvisionVO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProvisionVO.class)})
    @Secured({"ROLE_CARTERA"})
    @PostMapping("/create-multiple-provision")
    public ResponseEntity<?> createMultipleProvision(@Valid  @RequestBody List<FileProvisionVO> fileProvisionVO) {
        HashMap<String, Serializable> map = new HashMap<String, Serializable>();
        List<String> result = this.provisionService.createMultipleProvision(fileProvisionVO);
        if (result.size() < 0 ) {
            map.put("error: ", "No se registraron provisiones");
            return new ResponseEntity<HashMap<String, Serializable>>(map,  HttpStatus.CONFLICT);
        }
        return new ResponseEntity<List<String>>(result, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Search created provisions", nickname = "searchProvisions",
            notes = "Search by request number or by customer's rut",
            response = ProvisionVO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProvisionVO.class)}
    )
    @Secured({"ROLE_MESA_HIPOTECARIA","ROLE_CARTERA"})
    @GetMapping("/search-provisions/solicitud-number/{solicitudNumber}/rut/{rut}")
    public ResponseEntity<?> searchProvisions(@PathVariable long solicitudNumber, @PathVariable long rut )  {
        List<ProvisionResponseVO> provicionResponse;
        Map<String, Object> response = new HashMap<>();
        try {
            provicionResponse = provisionService.findProvisions(solicitudNumber, rut);
        } catch(Exception e) {
            response.put("mensaje", "Failed to query the database");
            response.put("error", e.getMessage());
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(provicionResponse.size() == 0) {
            response.put("mensaje", "there are no provisions for the search ");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ProvisionResponseVO>>(provicionResponse, HttpStatus.OK);
    }
    
  //Buscar provisiones por fecha contable
  	@ApiOperation(value = "Search provisions by accounting date", nickname = "searchProvisionsDate",
  			notes = "Search provisions by accounting date",
  			response = ProvisionVO.class)
  	@ApiResponses(value = {
  			@ApiResponse(code = 200, message = "OK", response = ProvisionVO.class)}
  	)
  	@Secured({"ROLE_CARTERA"})
  	@GetMapping("/search-prov-date/{startDate}/{endDate}")
  	public ResponseEntity<?> searchProvisionsDate(@PathVariable String startDate, @PathVariable String endDate )  {
  		List<ProvisionResponseVO> provicionResponse;
  		Map<String, Object> response = new HashMap<>();
  		try {
  			provicionResponse = provisionService.findByRangoFechaContable(startDate, endDate);			
  		} catch(Exception e) {
  			response.put("mensaje", "Failed to query the database");
  			response.put("error", e.getMessage());
  			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  		}
  		if(provicionResponse.size() == 0) {
  			response.put("mensaje", "No existen provisiones para la fecha indicada");
  			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
  		}
  		return new ResponseEntity<List<ProvisionResponseVO>>(provicionResponse, HttpStatus.OK);
  	}
  	
  	  
   @GetMapping("/download/{filename}")	 
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {	    	  
    	Resource resource =  this.provisionService.downloadFile(filename);
  	    return ResponseEntity.ok()
  	                .header(HttpHeaders.CONTENT_DISPOSITION,
  	                        "attachment; filename=\"" + resource.getFilename() + "\"")
  	                .body(resource);   
    }
}
