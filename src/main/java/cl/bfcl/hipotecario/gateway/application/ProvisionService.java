package cl.bfcl.hipotecario.gateway.application;

import cl.bfcl.hipotecario.gateway.exception.ErrorConnectionService;
import cl.bfcl.hipotecario.gateway.shared.vo.FileProvisionVO;
import cl.bfcl.hipotecario.gateway.shared.vo.ProvisionResponseVO;
import cl.bfcl.hipotecario.gateway.shared.vo.ProvisionVO;
import cl.bfcl.hipotecario.gateway.shared.vo.SolicitudVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class ProvisionService {
    private static Logger LOG = LoggerFactory.getLogger(ProvisionService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${url-servicio-hipotecario.request}")
    private String urlServiceRequest;

    @Value("${url-servicio-hipotecario.provision}")
    private String urlServiceProvision;

    public SolicitudVO findByNroSolicitud(BigDecimal numeroSolicitud) {
        try {
            String url = urlServiceRequest + "search-request/" + numeroSolicitud;
            SolicitudVO solicitudLegado = restTemplate.getForObject(urlServiceRequest + "search-request/" + numeroSolicitud, SolicitudVO.class);
            if (solicitudLegado!=null ) {
                LOG.info("----------------------- Servicio Hipotecario Provision  ---------------------");
                LOG.info(solicitudLegado.toString());
                LOG.info("----------------------- Servicio Hipotecario Provision ---------------------");
                return solicitudLegado;
            }
            throw new ErrorConnectionService("Error en MS Hipotecario Provision");
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio que obtiene la solicitud no responde");
        }

    }

    public List<String> createMultipleProvision(List<FileProvisionVO> fileProvisionVO) {
        try {
            List<String> result = restTemplate.postForObject(urlServiceProvision +  "create-multiple-provision/", fileProvisionVO, List.class);
            if (result.size() > 0 ) {
                LOG.info("----------------------- Servicio Hipotecario Provision  ---------------------");
                LOG.info(result.toString());
                LOG.info("----------------------- Servicio Hipotecario Provision ---------------------");
                return result;
            }
            throw new ErrorConnectionService("Error en MS Hipotecario Provision");
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio para crear la provision no response");
        }
    }

    public ProvisionVO createProvision(ProvisionVO provisionVO) {
        try {
            ProvisionVO req = restTemplate.postForObject(urlServiceProvision +  "create-provision/", provisionVO, ProvisionVO.class);
            if (req!=null ) {
                LOG.info("----------------------- Servicio Hipotecario Provision  ---------------------");
                LOG.info(req.toString());
                LOG.info("----------------------- Servicio Hipotecario Provision ---------------------");
                return req;
            }
            throw new ErrorConnectionService("Error en MS Hipotecario Provision");
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio para crear la provision no response");
        }
    }
    public List<ProvisionResponseVO> findProvisions(long solicitudNumber, long rut) {
        try {
            List<ProvisionResponseVO> provisionResponse = (List<ProvisionResponseVO>) restTemplate.getForObject(urlServiceProvision + "search-provisions/solicitud-number/" + solicitudNumber + "/rut/" + rut, List.class);
            if (provisionResponse.size() != 0 ) {
                LOG.info("----------------------- Servicio Hipotecario Provision  ---------------------");
                LOG.info(provisionResponse.toString());
                LOG.info("----------------------- Servicio Hipotecario Provision ---------------------");
                return provisionResponse;
            }
            throw new ErrorConnectionService("Error en MS Hipotecario Provision");
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio que obtiene la solicitud no responde");
        }

    }
    
    public List<ProvisionResponseVO> findByRangoFechaContable(String startDate, String endDate) {
        try {
            List<ProvisionResponseVO> provisionResponse = (List<ProvisionResponseVO>) restTemplate.getForObject(urlServiceProvision + "/search-prov-date/" + startDate + "/" + endDate, List.class);
            
            return provisionResponse;
          
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio Provision no responde");
        }

    }
   
    public Resource downloadFile(String filename) {
        try {
        	Resource resource = restTemplate.getForObject(urlServiceProvision + "/download/" + filename , Resource.class);
            
            return resource;
          
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio Provision no responde");
        }
    } 
}
