package cl.bfcl.hipotecario.gateway.application;

import cl.bfcl.hipotecario.gateway.exception.ErrorConnectionService;
import cl.bfcl.hipotecario.gateway.shared.vo.LoginVO;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import java.nio.charset.Charset;

@Service
public class LoginService {
    private static Logger LOG = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${url-servicio-hipotecario.login}")
    private String urlServiceLogin;


    public LoginVO userLogin(String userName, String pwd) {
        try {
            HttpHeaders headers = new HttpHeaders();
            HttpEntity request;
            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("username", userName);
            body.add("password", pwd);
            body.add("grant_type", "password");
            headers = createHeaders();
            request = new HttpEntity<Object>(body, headers);


            String url = urlServiceLogin;
            LoginVO loginVO = restTemplate.postForObject(urlServiceLogin, request, LoginVO.class);
            if (loginVO!=null ) {
                LOG.info("----------------------- Servicio Hipotecario Provision  ---------------------");
                LOG.info(loginVO.toString());
                LOG.info("----------------------- Servicio Hipotecario Provision ---------------------");
                return loginVO;
            }
            throw new ErrorConnectionService("Error en MS Hipotecario Provision");
        }catch (Exception e){
            LOG.info(e.getMessage());
            throw new ErrorConnectionService("El MicroServicio que obtiene la solicitud no responde");
        }
    }

    public HttpHeaders createHeaders() {
        return new HttpHeaders() {{
            String auth = "frontendapp" + ":" + "12345";
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
            setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        }};
    }
}
